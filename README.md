i3-ipc fun
==========

**CAUTION**: Wheel is reinvented everywhere in this repository.

There is:

* Reading JSON.
* Generating JSON.
* Communicating through Sockets via i3-ipc protocol.

And none of that involves any dependency. So don't take this seriously.
