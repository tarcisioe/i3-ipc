#ifndef SOCKET_CONSTANTS_H
#define SOCKET_CONSTANTS_H

#include <sys/socket.h>

namespace cpp {
namespace socket {

namespace detail {
constexpr auto MACRO_AF_INET = AF_INET;
constexpr auto MACRO_AF_UNIX = AF_UNIX;
constexpr auto MACRO_AF_LOCAL = AF_LOCAL;
}

#undef AF_INET
#undef AF_UNIX
#undef AF_LOCAL

enum class Family : short unsigned {
    AF_LOCAL = detail::MACRO_AF_LOCAL,
};

enum class Type {
    SOCK_STREAM = SOCK_STREAM,
};

}
}

#endif
