#ifndef SOCKET_SOCKET_H
#define SOCKET_SOCKET_H

#include <sys/socket.h>
#include <sys/un.h>
#include <vector>
#include <string>

#include "enum.h"
#include "constants.h"

extern "C" {
extern int close(int);
}

namespace cpp {
namespace socket {

class Socket {
public:
    Socket(Family family, Type type) :
        fd{::socket(utils::value(family), utils::value(type), 0)},
        family{family},
        type{type}
    {}

    ~Socket()
    {
        ::close(fd);
    }

    void connect(const std::string &address);

    void send(const std::string &buffer);
    void send(const std::vector<char> &buffer);
    void send(const void *data, std::size_t len);
    std::vector<char> receive(std::size_t length);

private:
    int fd;
    Family family;
    Type type;
};

}
}

#endif
