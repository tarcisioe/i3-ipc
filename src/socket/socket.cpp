#include "socket.h"

#include <unistd.h>
#include <stdexcept>

using namespace utils;

namespace cpp {
namespace socket {

void Socket::connect(const std::string &address)
{
    auto addr = ::sockaddr_un{value(family), ""};
    strncpy(addr.sun_path, address.c_str(), sizeof(addr.sun_path)-1);
    ::connect(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr));
}

void Socket::send(const std::string &buffer)
{
    if (!buffer.empty()) {
        auto bytes = ::write(fd, buffer.c_str(), buffer.size());

        if (std::size_t(bytes) != buffer.size()) {
            throw std::runtime_error{"connection error on send"};
        }
    }
}

void Socket::send(const std::vector<char> &buffer)
{
    if (!buffer.empty()) {
        auto bytes = ::write(fd, buffer.data(), buffer.size());

        if (std::size_t(bytes) != buffer.size()) {
            throw std::runtime_error{"connection error on send"};
        }
    }
}

void Socket::send(const void *data, std::size_t length)
{
    if (length) {
        auto bytes = ::write(fd, data, length);

        if (std::size_t(bytes) != length) {
            throw std::runtime_error{"connection error on send"};
        }
    }
}

std::vector<char> Socket::receive(std::size_t length)
{
    auto buf = std::vector<char>(length);

    auto bytes = ::read(fd, buf.data(), length);

    if (std::size_t(bytes) != length) {
        throw std::runtime_error{"connection error on receive"};
    }

    return buf;
}

}
}
