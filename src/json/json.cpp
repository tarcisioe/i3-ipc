#include "json.h"

#include <sstream>
#include <iomanip>

namespace {
    template <typename It>
    struct irange_t {
        irange_t(It b, It e):
            b{b}, e{e}
        {}

        It begin() const
        {
            return b;
        }

        It end() const
        {
            return e;
        }

        It b, e;
    };

    template <typename It>
    auto irange(It b, It e)
    {
        return irange_t<It>(b, e);
    }
}

namespace json {

std::string to_json(const std::string &s)
{
    std::stringstream ss{};
    ss << std::quoted(s);
    return ss.str();
}

std::string to_json(long int i)
{
    return std::to_string(i);
}

std::string to_json(double d)
{
    std::stringstream ss{};
    ss << d;
    return ss.str();
}

std::string to_json(bool b)
{
    std::stringstream ss{};
    ss << std::boolalpha << b;
    return ss.str();
}

std::string to_json(const Array& json)
{
    std::stringstream ss{};
    ss << '[';
    auto it = begin(json);

    if (it != end(json)) {
        ss << it->dump();
        ++it;
    }

    for (auto &j: irange(it, end(json))) {
        ss << ", " << j.dump();
    }

    ss << ']';

    return ss.str();
}

std::string to_json(const Object& json)
{
    std::stringstream ss{};

    ss << '{';
    auto it = begin(json);

    if (it != end(json)) {
        ss << to_json(it->first) << ": " << it->second.dump();
        ++it;
    }

    for (auto &j: irange(it, end(json))) {
        ss << ", " << to_json(j.first) << ": " << j.second.dump();
    }

    ss << '}';

    return ss.str();
}


}
