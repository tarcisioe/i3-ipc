#include "collection_utils.h"
#include "json.h"

using namespace utils;

namespace json {

    template <typename F>
        struct Deferrer {
            constexpr Deferrer(F f):
                f{std::move(f)}
            {}

            ~Deferrer()
            {
                f();
            }

            F f;
        };

    template <typename F>
    auto defer(F f)
    {
        return Deferrer<F>(std::move(f));
    }

    class Parser {
    public:
        Parser(const std::string& contents):
            contents{contents},
            position{0}
        {}

        JSON parse() const
        {
            position = 0;
            return thing();
        }

    private:
        char current() const
        {
            if (position < contents.size())
                return contents[position];
            return '\0';
        }

        bool expect(const std::string &what) const
        {
            skip_whitespace();
            auto on_end = defer([&](){skip_whitespace();});
            auto where = contents.find(what, position);
            if (where == position) {
                position += what.size();
                return true;
            }
            return false;
        }

        char expect_one_of(const std::string &possibilities) const
        {
            skip_whitespace();
            for (auto ch: possibilities) {
                if (current() == ch) {
                    ++position;
                    skip_whitespace();
                    return ch;
                }
            }
            return '\0';
        }

        JSON thing() const
        {
            skip_whitespace();
            switch (current()) {
                case '"':
                    return string();
                case '{':
                    return object();
                case '[':
                    return array();
                case '-':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    return number();
                case 't':
                case 'f':
                    return boolean();
                case 'n':
                    return null();
                default:
                    return JSON{};
            }
        }

        JSON null() const
        {
            position += 4;
            return JSON{};
        }

        JSON number() const
        {
            auto end = contents.find_first_not_of("-0123456789.", position);
            auto value_str = contents.substr(position, end-position);
            if (contains(value_str, '.')) {
                position = end;
                return JSON{std::stod(value_str)};
            } else {
                position = end;
                return JSON{std::stol(value_str)};
            }
        }

        JSON object() const
        {
            expect("{");
            Object obj;
            while (current() != '}') {
                skip_whitespace();

                if (current() != '"') {
                    return JSON{};
                }

                auto id = std_string();

                expect(":");
                skip_whitespace();

                obj[id] = thing();

                auto ch = expect_one_of(",}");

                if (ch == '}') {
                    break;
                } else if (ch != ',') {
                    return JSON{};
                }
            }
            return JSON{move(obj)};
        }

        JSON boolean() const
        {
            if (expect("true")) {
                return JSON{true};
            } else if (expect("false")) {
                return JSON{false};
            } else {
                return JSON{};
            }
        }

        JSON array() const
        {
            expect("[");
            auto arr = Array{};

            if (current() == ']') {
                ++position;
                return arr;
            }

            while (current() != ']') {
                skip_whitespace();
                arr.push_back(thing());

                auto ch = expect_one_of(",]");

                if (ch == ']') {
                    break;
                } else if (ch != ',') {
                    return JSON{};
                }
            }

            return JSON{move(arr)};
        }

        JSON string() const
        {
            auto str = std_string();
            return JSON{move(str)};
        }

        std::string std_string() const {
            expect("\"");
            auto start = position;
            while (current() != '"') { // totally ignores escapes
                ++position;
            }
            auto ret = contents.substr(start, position-start);
            ++position; // consume last "
            return ret;
        }

        void skip_whitespace() const
        {
            while (contains(" \t\n", current())) {
                ++position;
            }
        }

        const std::string &contents;
        mutable std::size_t position;
    };

    JSON loads(const std::string &contents)
    {
        return Parser(contents).parse();
    }
}
