#ifndef JSON_JSON_H
#define JSON_JSON_H

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "clonable.h"

namespace json {
    class JSON;

    using Array = std::vector<JSON>;
    using Object = std::map<std::string, JSON>;

    std::string to_json(const Array&);
    std::string to_json(const Object&);

    std::string to_json(const std::string&);
    std::string to_json(long int);
    std::string to_json(double);
    std::string to_json(bool);

    class JSON {
    public:
        JSON(): inner{nullptr} {}
        JSON(std::nullptr_t): JSON{} {}

        template <typename T>
        JSON(T x):
            inner{std::make_unique<Inner<T>>(std::move(x))}
        {}

        JSON(const JSON &other):
            inner{other.inner ? clone(*other.inner) : nullptr}
        {}

        JSON &operator=(const JSON &other)
        {
            return *this = JSON{other};
        }

        JSON(JSON &&) noexcept = default;
        JSON &operator=(JSON &&) noexcept = default;

        std::string dump() const
        {
            return inner ? inner->dump() : "null";
        }

        auto begin() const
        {
            return std::begin(as<Array>());
        }

        auto end() const
        {
            return std::end(as<Array>());
        }

        const auto &operator[](std::size_t i) const
        {
            return as<Array>()[i];
        }

        const auto &operator[](const std::string &s) const
        {
            return as<Object>().at(s);
        }

        template <typename T>
        auto is() const
        {
            return bool(dynamic_cast<const Inner<T>*>(inner.get()));
        }

        template <typename T>
        const T &as() const
        {
            return dynamic_cast<const Inner<T>&>(*inner).obj;
        }

        template <typename T>
        explicit operator T() const
        {
            return as<T>();
        }

    private:
        struct InnerBase: utils::Clonable<InnerBase> {
            virtual ~InnerBase() = default;
            virtual std::string dump() const = 0;
        };

        template <typename T>
        struct Inner final: utils::Clonable<InnerBase, Inner<T>> {
            Inner(T obj): obj(std::move(obj)) {}

            std::string dump() const override
            {
                return to_json(obj);
            }

            T obj;
        };

        std::unique_ptr<InnerBase> inner;
    };

    inline std::string dumps(const JSON &j)
    {
        return j.dump();
    }

    JSON loads(const std::string&);

    template <typename T>
    const T &cast(const JSON &j)
    {
        return j.as<T>();
    }

    template <typename T>
    auto is(const JSON &j)
    {
        return j.is<T>();
    }
}

#endif
