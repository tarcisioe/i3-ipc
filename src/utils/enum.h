#ifndef UTILS_ENUM_H
#define UTILS_ENUM_H

namespace utils {
    template <typename Enum>
    constexpr auto value(Enum e) {
        return std::underlying_type_t<Enum>(e);
    }
}

#endif
