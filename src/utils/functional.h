#ifndef UTILS_UTILS_H
#define UTILS_UTILS_H

template <typename T, typename F>
void for_enum(T &container, F op)
{
    auto i = std::size_t{};
    for (auto &v: container) {
        op(i++, v);
    }
}

#endif
