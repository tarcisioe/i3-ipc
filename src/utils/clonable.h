#ifndef UTILS_CLONABLE_H
#define UTILS_CLONABLE_H

namespace utils {
    template <typename Base, typename Derived = Base>
    class Clonable: public Base {
    public:
        using Base::Base;

        virtual ~Clonable() {}

        friend auto clone(const Clonable &clonable) {
            return std::unique_ptr<Derived>{dynamic_cast<Derived*>(clonable.clone())};
        }
    private:
        virtual Base *clone() const override {
            return new Derived(dynamic_cast<const Derived&>(*this));
        }
    };

    template <typename Base>
    class Clonable<Base, Base> {
    public:
        virtual ~Clonable() {}

        friend auto clone(const Clonable &clonable) {
            return std::unique_ptr<Base>{clonable.clone()};
        }

    private:
        virtual Base *clone() const = 0;
    };
}

#endif
