#ifndef UTILS_STATIC_STRING_H
#define UTILS_STATIC_STRING_H

#include <cstddef>

namespace meta {

template <std::size_t I>
struct size_t {
    using value_type = std::size_t;
    static constexpr auto value = I;
};

template <std::size_t ...Ns>
struct indices {
    template <std::size_t I>
    static constexpr auto append(meta::size_t<I>)
    {
        return indices<Ns..., I>{};
    }
};

constexpr indices<> make_indices(meta::size_t<0>)
{

}

template <std::size_t I>
constexpr auto make_indices(meta::size_t<I>)
{
    using prev = meta::size_t<I-1>;
    return make_indices(prev{}).append(prev{});
}

template <std::size_t N>
class string {
    static constexpr auto data_size = N+1;
public:
    using data_type = const char(&) [data_size];

    constexpr string(const char (&str) [data_size]):
        string(str, meta::make_indices(meta::size_t<N>{}))
    {}

    constexpr int size() const
    {
        return N;
    }

    constexpr data_type c_str() const
    {
        return data;
    }

    constexpr auto operator[](std::size_t n)
    {
        return data[n];
    }

private:
    template <std::size_t I, std::size_t ...Ns>
    constexpr string(const char (&str) [I], meta::indices<Ns...>):
        data{str[Ns]..., '\0'}
    {}

    char data[data_size];
};

template <int N>
constexpr auto make_string(const char (&str) [N])
{
    return string<N-1>(str);
}

}
#endif
