#ifndef UTILS_COLLECTION_UTILS_H
#define UTILS_COLLECTION_UTILS_H

#include <string>

namespace utils {
    inline bool contains(const std::string& str, char c)
    {
        return str.find(c) != std::string::npos;
    }
}

#endif
