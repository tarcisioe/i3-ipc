#ifndef I3_I3_CONNECTION_H
#define I3_I3_CONNECTION_H

#include <array>

// #include "enum.h"
#include "socket.h"
#include "json.h"

#include <iostream>

namespace i3 {
    enum class MessageType {
        COMMAND = 0,
        GET_WORKSPACES = 1,
        SUBSCRIBE = 2,
        GET_OUTPUTS = 3,
        GET_TREE = 4,
        GET_MARKS = 5,
        GET_BAR_CONFIG = 6,
        GET_VERSION = 7,
    };

    enum class ReplyType {
        COMMAND = 0,
        WORKSPACES = 1,
        SUBSCRIBE = 2,
        OUTPUTS = 3,
        TREE = 4,
        MARKS = 5,
        BAR_CONFIG = 6,
        VERSION = 7,
    };

    constexpr auto MAGIC_SIZE = 6;
    constexpr auto LENGTH_SIZE = 4;
    constexpr auto TYPE_SIZE = 4;

    struct IPCHeader {
        IPCHeader(MessageType type, std::size_t length)
        {
            *reinterpret_cast<std::uint32_t*>(&data[MAGIC_SIZE]) = length;
            *reinterpret_cast<std::uint32_t*>(&data[MAGIC_SIZE + LENGTH_SIZE]) = utils::value(type);
        }

        std::array<char, MAGIC_SIZE+LENGTH_SIZE+TYPE_SIZE> data{"i3-ipc"};
    };

    class Connection {
    public:
        Connection();

        void connect(const std::string &address);

        auto get_workspaces()
        {
            send(MessageType::GET_WORKSPACES);
            return json::loads(receive());
        }

        auto get_tree()
        {
            send(MessageType::GET_TREE);
            auto received = receive();
            auto j = json::loads(received);
            return j;
        }

    private:
        void send(MessageType, const std::string &data = "");
        std::string receive();

        cpp::socket::Socket socket;
    };
}

#endif
