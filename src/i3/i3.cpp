#include "i3.h"

using namespace cpp::socket;

namespace i3 {
    Connection::Connection():
        socket{Family::AF_LOCAL, Type::SOCK_STREAM}
    {}

    void Connection::connect(const std::string &address)
    {
        socket.connect(address);
    }

    void Connection::send(MessageType type, const std::string &message)
    {
        auto header = IPCHeader{type, message.size()};

        socket.send(header.data.data(), header.data.size());
        socket.send(message);
    }

    std::string Connection::receive()
    {
        auto response_header = socket.receive(6);
        auto length_buffer = socket.receive(4);
        auto type_buffer = socket.receive(4);

        auto length = *reinterpret_cast<std::uint32_t*>(length_buffer.data());

        auto payload_buffer = socket.receive(length);
        auto payload = std::string{payload_buffer.data(),
                                   payload_buffer.size()};

        return payload;
    }
}
