#include <algorithm>
#include <iostream>
#include <tuple>

#include "i3.h"
#include "json.h"

template <typename T>
auto pop(T &t)
{
    auto x = std::move(t.back());
    t.pop_back();
    return x;
}

template <typename T, typename U>
auto extend(T &t, const U &u)
{
    using std::begin;
    using std::end;
    t.insert(end(t), begin(u), end(u));
}

template <typename T, typename F>
auto find_if(T &&t, F &&pred)
{
    return std::find_if(std::begin(std::forward<T>(t)), std::end(std::forward<T>(t)), std::forward<F>(pred));
}

template <typename F>
auto search_tree(i3::Connection &i, F pred)
{
    auto tree = i.get_tree();

    auto nodes = json::Array{tree};
    auto found = json::Array{};

    while (not nodes.empty()) {
        auto node = pop(nodes);

        extend(nodes, node["nodes"]);
        extend(nodes, node["floating_nodes"]);

        if (pred(node)) {
            found.push_back(node);
        }
    }

    return found;
}

auto by_id(i3::Connection &i, long id)
{
    return search_tree(i, [&](const auto &node)
    {
        auto &window = node["window"];
        return json::is<long>(window) and long(window) == id;
    });
}

auto current_workspace_size(i3::Connection& i) {
    auto wss = i.get_workspaces();

    auto &workspace = *find_if(wss, [](auto w)
    {
        return w["focused"];
    });

    auto x = long(workspace["rect"]["x"]);
    auto y = long(workspace["rect"]["y"]);
    auto w = long(workspace["rect"]["width"]);
    auto h = long(workspace["rect"]["height"]);

    return std::make_tuple(x, y, w, h);
}

int main()
{
    using namespace std::string_literals;
    auto i = i3::Connection{};

    i.connect("/var/run/user/1000/i3/ipc-socket.4113");

    auto x = 0, y = 0, w = 0, h = 0;
    std::tie(x, y, w, h) = current_workspace_size(i);

    std::cout << x << ' ' << y << ' ' << w << ' ' << h << '\n';

    //auto j = i.get_tree()t;

    auto j = json::loads("{\"x\": []}"s);

    std::cout << json::dumps(j) << std::endl;

    auto windows = by_id(i, 0x3200007);

    for (auto &window: windows) {
        std::cout << long(window["rect"]["x"]) << std::endl;
    }
}
